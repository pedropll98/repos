﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBehaviour : MonoBehaviour {

    public Rigidbody rb;
    public Animator anim;
    public Camera cam;

    public Transform groundCheck;
    public float speedMultiplier;

    public bool run;
    public LayerMask whatIsGround;
    public bool isGrounded;
    public bool canPlane;
    public int n_saltos;
    public bool attacking;

    public GameObject[] trailPoints;
    public bool trailActivated;

    //Movement System
    public MovementSystem movementManager;
    //Air System
    public AirMovementSystem airMovementSystem;
    //Jumping Manager
    public JumpingManager jumpManager;
    //Dashing Manager
    public DashManager dashManager;
    //Stamina System
    public Stamina stamina;
    //Health System
    public HealthSystem healthManager;
    //Paraglider System
    public Paraglider paraglider;
    //Weapon System
    public WeaponSystem weaponManager;
    //Gadget System
    public GadgetSystem gadgetManager;
    
    private void Start () {
        healthManager.Start();
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
        cam = Camera.main;
	}
	
	void Update () {
        movementManager.speedMultiplier = speedMultiplier;
        airMovementSystem.speedMultiplier = speedMultiplier;

        IsGrounded();
        anim.SetBool("isGrounded", isGrounded);
        jumpManager.Update(ref rb, isGrounded);

        if(!dashManager.isDashing && Input.GetButtonDown("Circulo"))
        {
            dashManager.Activate(transform, rb);
            stamina.Stamina_waste(dashManager.waste);
        }

        //Freeze Y
        if (isGrounded)
        {
            jumpManager.isPlaning = false;
        }

        if (jumpManager.isPlaning)
        {
            stamina.Stamina_waste(0.5f);
            if (paraglider.isActive == false)
                paraglider.Start(rb);
            paraglider.isActive = true;
        }
        else
        {
            paraglider.isActive = false;
        }

        if (isGrounded)
        {
            Stamina_wait(0.5f);
        }

        if(isGrounded)
        {
            movementManager.isActive = true;
            airMovementSystem.isActive = false;
            paraglider.isActive = false;
        }
        else
        {
            airMovementSystem.isActive = true;
            movementManager.isActive = false;
        }
        
        if(movementManager.isActive)
        {
            movementManager.Update();
            movementManager.UpdateAnimator(anim);
        }

        if(jumpManager.isJumping)
        {
            airMovementSystem.cur_speed = movementManager.cur_speed;
        }

        if (airMovementSystem.isActive)
        {
            airMovementSystem.Update();
        }

        if (!airMovementSystem.isActive && !attacking && Input.GetButtonDown("Cuadrado"))
        {
            anim.SetBool("Attack", true);
            attacking = true;
        }

        if(dashManager.isActive)
        {
            dashManager.Update(anim);
        }
        
        if(dashManager.isDashing || movementManager.status == MOVE_STATE.SPRINTING)
        {
            ActivateTrailPoints();
        }
        else
        {
            DesActivateTrailPoints();
        }

        stamina.Update();
        healthManager.Update();

        anim.SetFloat("SpeedY", rb.velocity.y);
        anim.SetBool("Planning", jumpManager.isPlaning);
    }

    private void FixedUpdate()
    {
        if(movementManager.isActive)
            movementManager.FixedUpdate(rb, transform, cam);

        if (airMovementSystem.isActive)
            airMovementSystem.FixedUpdate(rb, transform, cam);

        if(jumpManager.isJumping)
        {
            jumpManager.FixedUpdate(rb);
        }

        if(jumpManager.isPlaning)
        {
            paraglider.FixedUpdate(rb);
        }

        if(dashManager.isDashing)
        {
            dashManager.FixedUpdate(rb);
        }
    }
    
    public void ActivateTrailPoints()
    {
        if(trailActivated == false)
            foreach (GameObject t in trailPoints)
            {
                t.SetActive(true);
            }

        trailActivated = true;
    }

    public void DesActivateTrailPoints()
    {
        if(trailActivated == true)
            foreach(GameObject t in trailPoints)
            {
                t.SetActive(false);
            }

        trailActivated = false;
    }

    //Aditional Stamina Functions
    public void Stamina_wait(float seconds)
    {
        stamina.seconds = seconds;
        StartCoroutine("Stamina_Wait");
    }

    public IEnumerator Stamina_Wait()
    {
        yield return new WaitForSeconds(stamina.seconds);
        if (!stamina.recovering) stamina.recovering = true;
    }

    //Jump Manager Functions
    public void IsGrounded ()
    {
        Debug.DrawRay(groundCheck.position, Vector3.down * 1f, Color.red, 0.2f);
        isGrounded = Physics.Raycast(groundCheck.position, Vector3.down, 1f, whatIsGround) && !jumpManager.isJumping;
    }
}


public enum MOVE_STATE { STANDING, WALKING, RUNNING, SPRINTING, SNEAKING }

[System.Serializable]
public class MovementSystem
{
    public float movH;
    public float movY;

    [HideInInspector]
    public float speedMultiplier;
    public float cur_speed;
    public float speed;
    public float mid_Speed;
    public float runSpeed;
    public float sneakSpeed;

    public MOVE_STATE status;

    public bool isActive;

    public void Update()
    {
        movH = Input.GetAxis("L_Hor");
        movY = Input.GetAxis("L_Ver");

        if(status != MOVE_STATE.SNEAKING)
        {
            if (Mathf.Abs(movH) <= 0.8f || Mathf.Abs(movY) <= 0.8f)
            {
                status = MOVE_STATE.WALKING;
            }

            if (Mathf.Abs(movH) > 0.8f || Mathf.Abs(movY) > 0.8f)
            {
                status = MOVE_STATE.RUNNING;
            }

            if (movH == 0 && movY == 0)
            {
                status = MOVE_STATE.STANDING;
            }

        }
        
        if (Input.GetButtonDown("L3"))
        {
            if(status == MOVE_STATE.SNEAKING)
            {
                status = MOVE_STATE.STANDING;
            }
            else if(status != MOVE_STATE.SNEAKING)
            {
                status = MOVE_STATE.SNEAKING;
            }
        }

        if(status != MOVE_STATE.STANDING)
        {
            if(Input.GetButton("R1"))
            {
                status = MOVE_STATE.SPRINTING;
            }
        }
    }

    public void FixedUpdate(Rigidbody rg, Transform transform, Camera cam)
    {
        Vector3 ver = movY * cam.transform.forward;
        Vector3 hor = movH * new Vector3(cam.transform.right.x, 0, cam.transform.right.z);

        Vector3 velocity = (hor + ver);

        velocity = velocity.normalized * cur_speed * speedMultiplier;

        Vector3 pointToLookAt = new Vector3(transform.position.x + velocity.x, transform.position.y, transform.position.z + velocity.z);

        rg.velocity = new Vector3(velocity.x, rg.velocity.y, velocity.z);

        if (pointToLookAt != transform.position)
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(pointToLookAt - transform.position, Vector3.up), 0.25f);
    }

    public void UpdateAnimator(Animator anim)
    {
        switch (status)
        {
            case MOVE_STATE.STANDING:
                cur_speed = 0;
                break;
            case MOVE_STATE.WALKING:
                cur_speed = speed;
                break;
            case MOVE_STATE.RUNNING:
                cur_speed = mid_Speed;
                break;
            case MOVE_STATE.SPRINTING:
                cur_speed = runSpeed;
                break;
            case MOVE_STATE.SNEAKING:
                cur_speed = sneakSpeed;
                break;
        }

        //TODO: Animator speed modify
        anim.SetFloat("Speed", cur_speed);
    }
}

[System.Serializable]
public class JumpingManager
{
    [SerializeField]
    private float count;
    [SerializeField]
    private float jumpForce;
    [SerializeField]
    private float sensibility;
    [SerializeField]
    private float maximumFallSpeed;
    [SerializeField]
    private int jumps;

    //public bool hadAirJump;
    //public bool canAirJump;
    public bool hasBeenReleased;
    public bool isJumping;
    public bool isPlaning;
    public bool isFlapping;

    public void Update(ref Rigidbody rb, bool isGrounded)
    {
        if (isGrounded)
            jumps = 1;

        if (rb.velocity.y < -maximumFallSpeed)
            jumps = 0;

        if (rb.velocity.y < 0)
            hasBeenReleased = true;

        if(jumps != 0 || isJumping || !hasBeenReleased)
        {
            if (Input.GetButtonDown("X"))
            {
                hasBeenReleased = false;
                count = 0;
                if (!isGrounded)
                    jumps = 0;
                isJumping = true;
            }

            if (Input.GetButton("X"))
            {
                count += Time.deltaTime;
                if (count < sensibility)
                {
                    isJumping = true;
                }
                if (count >= sensibility)
                {
                    isJumping = false;
                }

                if (count >= sensibility + 0.1f)
                {
                    isPlaning = true;
                }
            }

            if (Input.GetButtonUp("X"))
            {
                hasBeenReleased = true;
                isJumping = false;
                isPlaning = false;
            }
        }
        //TODO Fallos e implementacion de aleteo
        else if (jumps == 0 && !isGrounded)
        {
            if (Input.GetButton("X"))
            {
                isPlaning = true;
            }

            if (Input.GetButtonUp("X"))
            {
                isPlaning = false;
            }
        }
    }

    public void FixedUpdate(Rigidbody rb)
    {
        Vector3 velocity = rb.velocity;
        velocity.y = jumpForce;
        rb.velocity = velocity;
    }

}

[System.Serializable]
public class DashManager
{
    public bool isActive;
    public float dashSpeed;
    public bool isDashing;
    public float cur_duration;
    public float duration;
    public float waste;

    public Vector3 direction;

    public void Activate(Transform t, Rigidbody rb)
    {
        isActive = true;
        direction = t.forward;
        isDashing = true;
        cur_duration = 0;
    }

    public void Desactivate()
    {
        isActive = false;
    }

    public void Update(Animator anim)
    {
        cur_duration += Time.deltaTime;

        if(cur_duration >= duration)
        {
            isDashing = false;
            Desactivate();
        }

        anim.SetBool("isDashing", isDashing);
    }

    public void FixedUpdate(Rigidbody rb)
    {
        rb.velocity = direction * dashSpeed;
    }
}

[System.Serializable]
public class AirMovementSystem
{
    public float movH;
    public float movY;
    [HideInInspector]
    public float speedMultiplier;
    public float cur_speed;
    public float fallSpeed;
    public float planeSpeed;

    public bool isActive;

    public void Update()
    {
        movH = Input.GetAxis("L_Hor");
        movY = Input.GetAxis("L_Ver");
    }

    public void FixedUpdate(Rigidbody rg, Transform transform, Camera cam)
    {
        Vector3 ver = movY * cam.transform.forward;
        Vector3 hor = movH * new Vector3(cam.transform.right.x, 0, cam.transform.right.z);

        Vector3 velocity = (hor + ver);

        velocity = velocity.normalized * cur_speed * speedMultiplier;

        Vector3 pointToLookAt = new Vector3(transform.position.x + velocity.x, transform.position.y, transform.position.z + velocity.z);

        rg.velocity = new Vector3(velocity.x, rg.velocity.y, velocity.z);

        if (pointToLookAt != transform.position)
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(pointToLookAt - transform.position, Vector3.up), 0.25f);
    }

}

[System.Serializable]
public class Paraglider
{
    public float Force;
    public bool isActive;

    public void Start(Rigidbody rg)
    {
        Vector3 v = rg.velocity;
        v.y = -Force;
        rg.velocity = v;
    }

    public void End(Rigidbody rg)
    {
        Vector3 v = rg.velocity;
        v.y = 0;
        rg.velocity = v;
    }

    public void FixedUpdate(Rigidbody rg)
    {
        rg.AddForce(Vector3.up * -Physics.gravity.y * 0.9f, ForceMode.Force);
    }

}

[System.Serializable]
public class HealthSystem
{
    public float maxHealth;
    public float curHealth;

    public Image health;

    public void Start()
    {
        curHealth = maxHealth;
    }

    public void Update()
    {
        health.fillAmount = curHealth / maxHealth;
    }

    public void Damage(float amount)
    {
        curHealth -= amount;
        if (curHealth <= 0) curHealth = 0;
    }

    public void Heal (float amount)
    {
        curHealth += amount;
        if (curHealth >= maxHealth) curHealth = maxHealth;
    }
}

[System.Serializable]
public class Stamina
{
    [HideInInspector]
    public float puntos_de_stamina;
    public float puntos_de_stamina_maximos;
    public Image barra_stamina;
    public float seconds;

    public bool recovering;
    
    public void Update()
    {
        if (puntos_de_stamina > puntos_de_stamina_maximos)
        {
            puntos_de_stamina = puntos_de_stamina_maximos;
            recovering = false;
        }
        if (puntos_de_stamina <= 0) { puntos_de_stamina = 0; }

        barra_stamina.fillAmount = puntos_de_stamina / puntos_de_stamina_maximos;

        if (recovering)
        {
            Stamina_recover();
        }
    }

    void Stamina_recover()
    {
        puntos_de_stamina += 0.1f;
    }

    public void Stamina_waste(float waste)
    {
        recovering = false;
        puntos_de_stamina -= waste;
    }

}



[System.Serializable]
public class WeaponSystem
{
    //TODO
    public Weapon[] weapons;
    public int currentWeaponIndex;

    public Image right, left, current;

    public int rightIndex()
    {
        if(currentWeaponIndex == weapons.Length - 1)
        {
            return 0;
        }

        return currentWeaponIndex + 1;
    }

    public int leftIndex()
    {
        if (currentWeaponIndex == 0)
        {
            return weapons.Length - 1;
        }

        return currentWeaponIndex - 1;
    }
}



[System.Serializable]
public class Weapon
{
    //TODO
    //public int weaponIndex;
    //public float attack_force;
    //public float defense;
    //public float cur_resistance;
    //public float resistance;
    //public float stamina_waste; //Stamina waste attacking
    //public float stability;     //Parameter for stamina waste calculation
    //public bool isBroken;
    //public GameObject target;
    //public byte cur_level;
    //public const byte maxLevel = 5;
    //public bool isSelected;

    public int AnimatorIndex;
    public string name;


}

[System.Serializable]
public class Sword : Weapon
{
    //TODO

}

[System.Serializable]
public class GadgetSystem
{
    //TODO
}

//TODO NEXT CUATRI LOCO
[System.Serializable]
public class Gadget
{
    //TODO

}

[System.Serializable]
public class Hook : Gadget
{
    //TODO
}

[System.Serializable]
public class Bow : Gadget
{
    //TODO
}